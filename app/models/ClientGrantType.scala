package models

import models.database.ClientGrantTypes

case class ClientGrantType(clientId: String, grantTypeId: Long)

